import { Component, OnInit } from '@angular/core';
import { ProjectService, Project } from 'src/app/services/project.service';
import { Observable, interval } from 'rxjs';
import { map, throttleTime } from 'rxjs/operators';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {
  projects: Project[] = [];
  addNewState: Boolean = false;
  newName: String = '';

  constructor(public projectService: ProjectService) {
  }

  ngOnInit() {
    this.getProjects();
  }

  getProjects() {
    this.projectService.getProjects().subscribe((res) => {
      this.projects = res;
    });
  }

  startProject(id) {
    const project = this.projects.find(  (a) => {
      return a._id === id;
    });
    project.isActive = true;
    this.projectService.updateProject(id, project).subscribe(() => {
      this.getProjects();
    });
  }

  stopProject(id) {
    const project = this.projects.find(  (a) => {
      return a._id === id;
    });
    project.isActive = false;
    this.projectService.updateProject(id, project).subscribe(() => {
      this.getProjects();
    });
  }

  createProject() {
    const project = {
      name: this.newName,
      precission: false,
      isActive: false,
      questions: []
    };
    this.projectService.createProject(project).subscribe(() => {
      this.getProjects();
      this.addNewState = !this.addNewState;
    } );
  }

  deleteProject(id) {
    this.projectService.deleteProject(id).subscribe(() => {
      this.getProjects();
    });
  }
}
