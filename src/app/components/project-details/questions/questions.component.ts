import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.scss']
})
export class QuestionsComponent implements OnInit {
  @Input() questions = [];
  activeQuestion: Object = {};
  newQuestionName = '';

  constructor() { }

  ngOnInit() {
    if (this.questions.length > 0) { this.activeQuestion = this.questions[0]; }
  }

  setActiveQuestion(question) {
    this.activeQuestion = question;
  }

  addQuestion() {
    this.questions.push({
      name: this.newQuestionName,
      isActive: false,
      answers: []
    });
    this.newQuestionName = '';
  }

}
