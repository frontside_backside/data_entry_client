import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-question-details',
  templateUrl: './question-details.component.html',
  styleUrls: ['./question-details.component.scss']
})
export class QuestionDetailsComponent implements OnInit {
  @Input() question;

  list = [
    {value: 1, text: 'Variant'},
    {value: 2, text: 'Variant multi'},
    {value: 3, text: 'Number'},
    {value: 4, text: 'Open q'}
  ];

  newAnsVal;
  newAnsText = '';
  newAnswActive = false;

  constructor() { }

  ngOnInit() {
  }

  removeAnswer(answer) {
    const index = this.question.answers.indexOf(answer);
    this.question.answers.splice(index, 1);
  }

  addAnswer(value, displayText, isActive) {
    this.question.answers.push({
      value: value,
      displayText: displayText,
      isActive: isActive
    });
    this.newAnsVal = '';
    this.newAnsText = '';
    this.newAnswActive = false;
  }

}
