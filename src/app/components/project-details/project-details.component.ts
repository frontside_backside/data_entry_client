import { Component, OnInit } from '@angular/core';
import { ProjectService, Project } from 'src/app/services/project.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.scss']
})
export class ProjectDetailsComponent implements OnInit {
  project: Project;
  id: string;
  alertMessage: string;

  constructor(private route: ActivatedRoute, private router: Router, private projectService: ProjectService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params['id'];
      this.getProject(this.id);
    });
  }

  getProject(id) {
    this.projectService.getProject(id).subscribe(project => this.project = project );
  }

  saveProject(project) {
    this.projectService.updateProject(this.id, project).subscribe();
  }

  goBack() {
    this.router.navigateByUrl('/projects');
  }

}
