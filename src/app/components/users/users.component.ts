import { Component, OnInit } from '@angular/core';
import { ProjectService } from 'src/app/services/project.service';
import { UserService, User } from '../../services/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  projectList: Object[] = [];
  users: User[];
  addNewState = false;
  newName: string;
  newLogin: string;
  newRole: string;
  newPass: string;

  constructor(
    private userService: UserService,
    private projectService: ProjectService) { }

  ngOnInit() {
    this.getUsers();
    this.getProjects();
  }

  getUsers() {
    this.userService.getUsers().subscribe(users => this.users = users);
  }

  createUser() {
    this.userService.createUser({
      name: this.newName,
      login: this.newLogin,
      password: this.newPass,
      role: this.newRole
    }).subscribe(()=>{
      this.getUsers();
      this.newLogin = '',
      this.newName = '',
      this.newPass = '',
      this.newRole = '',
      this.addNewState = false
    });

  }

  updateUser(user) {
    this.userService.updateUser(user._id, user).subscribe();
  }

  deleteUser(id) {
    this.userService.deleteUser(id).subscribe(() => {
      this.getUsers();
    });
  }

  getProjects() {
    this.projectService.getProjects().subscribe(projects => this.projectList = projects.map((el) => {
      return {
        name: el.name,
        id: el._id
      };
    }));
  }

}
