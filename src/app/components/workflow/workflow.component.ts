import { Component, OnInit, OnDestroy } from '@angular/core';
import { ProjectService, Project, Question, Answer } from '../../services/project.service';
import { UserService, User } from '../../services/user.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-workflow',
  templateUrl: './workflow.component.html',
  styleUrls: ['./workflow.component.scss']
})
export class WorkflowComponent implements OnInit, OnDestroy {
  userServiceSubscription: Subscription;
  projectServiceSubscription: Subscription;
  lsUserId: string;
  currentUser: User;
  currentProject: Project = null;
  questions: Question[];
  activeQuestions: Question[];
  currentQuestion = null;
  currentQuestionNumber: number = 0;
  currentAnswer: any;
  prevAnswers = [];
  answers = [];
  nextBtnText: string = 'Next';

  constructor(private userService: UserService, private projectService: ProjectService) { }

  ngOnInit() {
    this.lsUserId = JSON.parse(localStorage.getItem('currentUser'))._id;
    this.userServiceSubscription = this.userService.getUser(this.lsUserId).subscribe(user => {
      if (user !== null) {
        this.currentUser = user;
        this.projectServiceSubscription = this.projectService.getProject(this.currentUser.task).subscribe(project => {
          if (project.name && project.isActive){
  
            this.currentProject = project;
            this.questions = this.currentProject.questions;
            if (this.questions.length) {
              this.activeQuestions = this.questions.filter( el => el.isActive === true );
              this.currentQuestion = this.questions[0];
              if (this.currentQuestion.type === 2) {
                this.currentAnswer = [];
              } else {
                this.currentAnswer = '';
              }
              this.currentQuestionNumber = 1;
            }

          }
        });
      }
    });
  }

  ngOnDestroy(){
    if(this.userServiceSubscription){
      this.userServiceSubscription.unsubscribe();
    }
    if(this.projectServiceSubscription){
      this.projectServiceSubscription.unsubscribe();
    }
  }

  setSingleAnswer(value, event) {
    this.currentAnswer = value;
    event.target.parentElement.parentElement.querySelectorAll('mat-list-item').forEach(el => {
      if (el.classList) {
        el.classList.remove('active');
      }
    });
    event.target.parentElement.classList.add('active');
  }

  setMultiAnswer(value, event) {
    const index = this.currentAnswer.indexOf(value);
    if (index > -1) {
      this.currentAnswer.splice(index, 1);
    } else {
      this.currentAnswer.push(value);
    }
    event.target.parentElement.classList.toggle('active');
  }

  nextQuestion() {
    const currentIndex = this.questions.indexOf(this.currentQuestion);
    this.currentQuestionNumber += 1;

    if (this.answers.length > currentIndex + 1) {
      this.answers[currentIndex] = this.currentAnswer;
      this.currentAnswer = this.answers[currentIndex + 1];
    } else if (this.answers.length > currentIndex) {
      this.answers[currentIndex] = this.currentAnswer;
      this.currentAnswer = '';
    } else {
      this.answers.push(this.currentAnswer);
      this.currentAnswer = '';
    }


    if (currentIndex === this.questions.length - 1) {
      this.saveDocument();
      this.currentQuestionNumber = 1;
      this.currentQuestion = this.questions[0];
      this.nextBtnText = 'Next';
      return;
    } else if (currentIndex === this.questions.length - 2) {
      this.nextBtnText = 'Save';
    } else if (this.activeQuestions.indexOf(this.questions[currentIndex]) === this.activeQuestions.length - 2 ) {
      this.nextBtnText = 'Save';
    }


    this.currentQuestion = this.questions[currentIndex + 1];
   if (!this.currentQuestion.isActive) {
    this.currentQuestionNumber -= 1;
    this.nextQuestion();
   }

    if (this.currentQuestion.type === 2) {
      this.currentAnswer = [];
    }
  }

  prevQuestion() {
    const currentIndex = this.questions.indexOf(this.currentQuestion);
    if (currentIndex > 0) {
      this.currentQuestion = this.questions[currentIndex - 1];
      this.currentAnswer = this.answers[currentIndex - 1];
      this.nextBtnText = 'Next';
      if (!this.currentQuestion.isActive) {
        this.prevQuestion();
        return;
      }
      this.currentQuestionNumber -= 1;
    }
  }

  saveDocument() {
    this.projectService.saveDocument(this.currentProject._id, this.answers).subscribe(data => console.log(data));
    this.prevAnswers = [...this.answers];
    this.answers = [];
  }

}
