import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { User } from '../../services/user.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  currentUser: User;
  loginBtnText: String;

  constructor(private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.authenticationService.currentUser.subscribe(x => {
      this.currentUser = x;
      if (x) {
        this.loginBtnText = 'Logout';
      } else {
        this.loginBtnText = 'Login';
      }
    } );
  }

  logout(event) {
    event.preventDefault();
    this.authenticationService.logout();
  }

}
