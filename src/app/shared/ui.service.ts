import { MatSnackBar } from '@angular/material/snack-bar';
import { Injectable } from '@angular/core';

@Injectable()
export class UIService {

    constructor(private snackBar: MatSnackBar) {}

    showSneakbar(message) {
        this.snackBar.open(message, null, {
            duration: 3000,
        });
    }
}