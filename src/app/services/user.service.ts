import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';

export interface User {
  _id: string;
  task: string;
  name: string;
  login: string;
  role: string;
  password: string;
  __v: string;
  token?: string;
}

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  constructor(private http: HttpClient) { }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(`${environment.host}/api/users`)
    .pipe(
      catchError(this.handleError('getUsers', []) )
    );
  }

  getUser(id): Observable<User> {
    return this.http.get<User>(`${environment.host}/api/users/${id}`)
    .pipe(
      catchError(this.handleError('getUsers', null) )
    );
  }

  createUser(user: Object): Observable<User> {
    return this.http.post<User>(`${environment.host}/api/users/`, user)
    .pipe(
      catchError(this.handleError('getUsers', null) )
    );
  }

  updateUser(id: String, user: User): Observable<User> {
    return this.http.put<User>(`${environment.host}/api/users/${id}`, user)
    .pipe(
      catchError(this.handleError('getUsers', null) )
    );
  }

  deleteUser(id: String): Observable<User> {
    return this.http.delete<User>(`${environment.host}/api/users/${id}`).pipe(
      catchError(this.handleError('getUsers', null) )
    );
  }
}
