import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';

export interface Project {
  _id: string;
  name: string;

  date: Date;
  isActive: boolean;
  precission: boolean;
  questions: Question[];
  count?: any;
  _v?: string;
}

export interface Question {
  displayText: String;
  isActive: Boolean;
  answers: Answer[];
  _id: String;
  type: Number;
  name: String;
}

export interface Answer {
  displayText: String;
  isActive: Boolean;
  _id: String;
  value: String;
}

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  constructor(private http: HttpClient) { }

  getProjects(): Observable<Project[]> {
    return this.http.get<Project[]>(`${environment.host}/api/projects`)
    .pipe(
      catchError(this.handleError('getProjects', []) )
    );
  }

  getProject(id: string): Observable<Project> {
    return this.http.get<Project>(`${environment.host}/api/projects/${id}`)
    .pipe(
      catchError(this.handleError('getProject', null) )
    );
  }

  createProject(project: object): Observable<Project> {
    return this.http.post<Project>(`${environment.host}/api/projects/`, project)
    .pipe(
      catchError(this.handleError('createProject', null) )
    );
  }

  updateProject(id: string, project: Project): Observable<Project> {
    return this.http.put<Project>(`${environment.host}/api/projects/${id}`, project)
    .pipe(
      catchError(this.handleError('updateProject', null) )
    );
  }

  deleteProject(id: string): Observable<Project> {
    return this.http.delete<Project>(`${environment.host}/api/projects/${id}`).pipe(
      catchError(this.handleError('deleteProject', null) )
    );
  }

  saveDocument(id: string, document: object): Observable<object> {
    return this.http.post<object>(`${environment.host}/api/projects/${id}/documents`, document);
  }

  countDocuments(id: string): Observable<any> {
    return this.http.get<any>(`${environment.host}/api/projects/${id}/count`).pipe(
      catchError(this.handleError('countDocuments', null) )
    );
  }
}
