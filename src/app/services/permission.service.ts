import { NgxPermissionsService } from 'ngx-permissions';
import { Injectable } from '@angular/core';

@Injectable()
export class PermissionService {

    constructor ( private permissionsService: NgxPermissionsService) {}

    public loadPermissions(role) {
        const permissions = [];
        if (role) {
            permissions.push(role);
        }
        this.permissionsService.loadPermissions(permissions);
    }
}