import { Component, OnInit, OnDestroy } from '@angular/core';
import { PermissionService } from './services/permission.service';
import { AuthenticationService } from './services/authentication.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy{
  title = 'data-entry-client';
  userSubscription: Subscription;

  constructor(private permissionService: PermissionService, private authService: AuthenticationService) {}

  ngOnInit() {
    this.userSubscription = this.authService.currentUser.subscribe(user => {
      if (user && user.role){
        this.permissionService.loadPermissions(user.role);
      } else {
        this.permissionService.loadPermissions(null);
      }
    });
  }

  ngOnDestroy() {
    if (this.userSubscription){
      this.userSubscription.unsubscribe();
    }
  }
  
}
