import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClient, HttpClientModule,  HTTP_INTERCEPTORS } from '@angular/common/http';
import { ProjectsComponent } from './components/projects/projects.component';
import { HeaderComponent } from './components/header/header.component';
import { ProjectDetailsComponent } from './components/project-details/project-details.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { UsersComponent } from './components/users/users.component';
import { FormsModule } from '@angular/forms';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { QuestionsComponent } from './components/project-details/questions/questions.component';
import { QuestionDetailsComponent } from './components/project-details/questions/question-details/question-details.component';
import { WorkflowComponent } from './components/workflow/workflow.component';
import { LoginComponent } from './components/login/login.component';

import { JwtInterceptor } from './helpers/jwt.interceptor';
import { ErrorInterceptor } from './helpers/error.interceptor';

import { NgxPermissionsModule } from 'ngx-permissions';
import { PermissionService } from './services/permission.service';
import { MaterialModule } from './material.module';
import { UIService } from './shared/ui.service';
@NgModule({
  declarations: [
    AppComponent,
    ProjectsComponent,
    HeaderComponent,
    ProjectDetailsComponent,
    DashboardComponent,
    UsersComponent,
    PageNotFoundComponent,
    QuestionsComponent,
    QuestionDetailsComponent,
    WorkflowComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    NgxPermissionsModule.forRoot(),
    MaterialModule
  ],
  providers: [
    HttpClient,
    PermissionService,
    UIService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
