import { NgModule } from '@angular/core';
import { MatSelectModule, MatFormFieldModule, MatCardModule, MatListModule, MatIconModule, MatButtonModule, MatDividerModule, MatSnackBarModule } from '@angular/material';

@NgModule({
    imports: [
        MatSelectModule,
        MatFormFieldModule,
        MatCardModule,
        MatListModule,
        MatIconModule,
        MatButtonModule,
        MatDividerModule,
        MatSnackBarModule
    ],
    exports: [
        MatSelectModule,
        MatFormFieldModule,
        MatCardModule,
        MatListModule,
        MatIconModule,
        MatButtonModule,
        MatDividerModule,
        MatSnackBarModule
    ]
})
export class MaterialModule {

}